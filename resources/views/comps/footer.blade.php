<footer class="footer bg-pattern">
    <div class="flex justify-around items-center">
        <div>
            <p class="text-sm leading-normal">
                Vanderlei Sbaraini Amancio <br>
                contato@artopus.com.br <br>
                +55 47 99691-1554
            </p>
        </div>
        <div>
            <a class="text-sm px-2" target="_blank" href="https://www.twitter.com/vander_dev">Twitter</a>
            <a class="text-sm px-2" target="_blank" href="https://www.linkedin.com/in/vanderleiamancio">LinkedIn</a>
            <a class="text-sm px-2" target="_blank" href="https://www.github.com/harrysbaraini">Github</a>
        </div>
    </div>
    <div class="text-center mt-8">
        <p class="text-xs">
            Vanderlei Sbaraini Amancio © 2018. {{ __('All rights reserved') }}.
        </p>
        <p class="text-xs mt-2">
            {{ __('Background pattern provided by') }} <a href="https://www.toptal.com/designers/subtlepatterns" target="_blank">Subtle Patterns</a>
        </p>
    </div>
</footer>