<div class="nav bg-pattern border-b border-grey-light">
    <nav class="max-w-2xl mx-auto flex items-center justify-between flex-wrap p-6">
        <div class="flex flex-no-shrink mr-6">
            <a href="{{ route('home', config('app.locale')) }}" class="no-underline font-mono font-normal text-xl tracking-tight text-brand-darker hover:text-brand-dark">{{ config('app.name') }}</a>
        </div>
        <div class="block flex flex-no-shrink items-center w-auto">
            <div class="text-sm flex-grow">
                <a href="{{ route('home', config('app.locale')) }}" class="no-underline inline-block font-mono font-light text-lg text-brand-darker hover:text-brand-dark mr-4">
                    {{ __('Posts') }}
                </a>
                <a href="{{ route('about', config('app.locale')) }}" class="no-underline inline-block font-mono font-light text-lg text-brand-darker hover:text-brand-dark mr-4">
                    {{ __('About') }}
                </a>
                <div class="border-l border-grey-light inline-block pl-4">
                    @foreach (config('blog.locales') as $locale)
                        <a href="{{ route('home', $locale) }}" class="no-underline inline-block font-mono font-light text-sm text-brand-darker hover:text-brand-dark mr-4">
                            {{ strtoupper($locale) }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </nav>
</div>