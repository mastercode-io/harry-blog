@extends('layouts.app')

@section('content')
    <div class="max-w-xl mx-auto mt-8 text-center">
        <h1 class="text-5xl font-bold leading-normal text-brand-darker py-3">
            {{ $post->title }}
        </h1>

        <small class="font-normal text-base text-grey-dark my-2 block">
            {{ __('Published on') }} {{ publishedOn($post->date) }}
        </small>
    </div>

    <div class="max-w-md mx-auto">
        <blockquote class="leading-tight italic font-serif text-xl text-grey-darker px-4 pb-4 my-8">
            {!! $post->summary !!}
        </blockquote>

        <div class="my-8 leading-normal post-content text-black">
            {!! $post->body !!}
        </div>
    </div>
@endsection