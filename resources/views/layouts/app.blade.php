<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('fonts/fira-code/fira-code.css') }}" rel="stylesheet">
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">
</head>
<body class="bg-white font-sans font-normal">
@component('comps.nav') @endcomponent

<div class="flex flex-col">
    <div class="min-h-screen flex justify-center mt-8">
        <div class="container h-full">
            @yield('content')
        </div>
    </div>
</div>

@include('comps.footer')

</body>
</html>
