@extends('layouts.app')

@section('content')

    <ul class="list-reset max-w-md mx-auto">
        @foreach ($posts as $post)
            <li class="mt-8 pb-6">
                <a href="{{ $post->url }}" class="block mb-1 no-underline text-3xl leading-normal font-bold text-brand-darkest hover:text-brand-dark" title="Documentation">
                    {{ $post->title }}
                </a>
                <small class="text-grey">{{ publishedOn($post->date) }}</small>
                <p class="mt-4 text-base text-grey-dark leading-normal">{{ $post->summary }}</p>
            </li>
        @endforeach
    </ul>

@endsection