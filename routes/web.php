<?php

use App\Http\Controllers\HomeHandler;
use App\Http\Controllers\ShowPostHandler;
use App\Http\Controllers\AboutHandler;
use App\Http\Middleware\ValidateLocale;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/{locale}')->middleware(ValidateLocale::class)->group(function () {
    Route::get('/', HomeHandler::class)->name('home');
    Route::get('/about', AboutHandler::class)->name('about');
    Route::get('/{slug}', ShowPostHandler::class)->name('posts.show');
});

Route::redirect('/', config('blog.default_locale'));
