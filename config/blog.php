<?php

return [
    /*
     * Locales available on the blog.
     */
    'locales' => [
        'en',
        'br',
    ],

    /*
     * Default locale for the blog.
     */
    'default_locale' => 'en',

    /*
     * Filesystem disk.
     */
    'disk' => 'posts',
];
