<?php

namespace App\Http\Middleware;

use Closure;
use Config;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Check if a locale was provided, otherwise redirect the blog
 * to the default locale.
 */
class ValidateLocale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = $request->segment(1);

        if (!in_array($locale, config('blog.locales'))) {
            throw new NotFoundHttpException();
        }

        Config::set('app.locale', $locale);
        \Carbon\Carbon::setLocale($locale);

        return $next($request);
    }
}
