<?php

namespace App\Http\Controllers;

use App\Content\Posts;

/**
 * Shows up the home page of the blog.
 */
class HomeHandler extends Controller
{
    public function __invoke(string $locale)
    {
        $items = Posts::allLocalized($locale);

        publishedOn($items->first()->date);

        return view('home')->withPosts($items);
    }
}
