<?php

namespace App\Http\Controllers;

class ShowPostHandler extends Controller
{
    public function __invoke(string $locale, string $slug)
    {
        $item = \App\Content\Posts::find($slug, $locale);

        return view('post')->withPost($item);
    }
}
