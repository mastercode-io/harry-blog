<?php

use League\CommonMark\CommonMarkConverter;

/**
 * Convert a markdown string to a HTML one.
 *
 * @param string $markdown
 * @return void
 */
function markdown($markdown)
{
    return app(CommonMarkConverter::class)->convertToHtml($markdown);
}

/**
 * Print out a date to the format used on "Published on" strings.
 *
 * @param mixed $date
 * @return string
 */
function publishedOn($date)
{
    if (config('app.locale') === 'en') {
        return $date->format('F Y');
    }

    $months = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    $monthLabel = $months[(int)$date->format('m') - 1];

    return "$monthLabel de {$date->format('Y')}";
}
