<?php

namespace App\Content;

use Illuminate\Contracts\Filesystem\Factory as Filesystem;
use Illuminate\Support\Collection;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostsManager
{
    /**
     * Filesystem disk.
     *
     * @var Filesystem
     */
    protected $disk;

    /**
     * Collection of posts.
     *
     * @var Collection
     */
    protected $items;

    /**
     * Constructor.
     *
     * @param Filesystem $filesystem
     */
    public function __construct(Filesystem $filesystem)
    {
        $this->disk = $filesystem->disk('posts');
    }

    /**
     * Get a list of all posts.
     *
     * @return Collection
     */
    public function all(): Collection
    {
        if (!$this->items) {
            $this->items = $this->readAllDirectories()->sortByDesc('date');
        }

        return $this->items;
    }

    /**
     * Get a list of all posts with a locale version.
     *
     * @param string $locale
     * @return Collection
     */
    public function allLocalized(string $locale): Collection
    {
        $items = $this->all();

        $items = $items->map(function ($item) use ($locale) {
            return $item->get($locale);
        })->filter();

        return $items;
    }

    /**
     * Paginate the posts.
     *
     * @param string  $locale
     * @param integer $perPage
     * @param string  $pageName
     * @param mixed   $page
     * @return void
     */
    public function paginate(string $locale, $perPage = 15, $pageName = 'page', $page = null)
    {
        return $this->allLocalized($locale)->simplePaginate($perPage, $pageName, $page);
    }

    /**
     * Find a post by its slug.
     *
     * @param string $locale
     * @param string $slug
     * @return void
     */
    public function find(string $slug, ?string $locale = null)
    {
        if (!$slug) {
            return;
        }

        // If the locale was provided we search the post only on the localized
        // posts. Otherwise, we will proceed to find the post without filtering
        // by locale.
        $collection = isset($locale) ? $this->allLocalized($locale) : $this->all()->flatten(1);
        $item = $collection->where('slug', $slug);

        // If anything is found, we throw out a exception to inform
        // that the post could not be found.
        if (is_null($item)) {
            throw new NotFoundHttpException();
        }

        return $item->first();
    }

    /**
     * Look for post directories.
     *
     * @return Collection
     */
    protected function readAllDirectories(): Collection
    {
        return collect($this->disk->directories())
            ->filter(function ($path) {
                return preg_match('/([0-9]{4})\-([0-1][1-9])\-([0-3][0-9])\..+/', $path);
            })
            ->mapWithKeys(function ($path) {
                [$date, $slug] = explode('.', $path, 2);
                return [$slug => $this->readDirectoryFiles($path)];
            });
    }

    protected function readDirectoryFiles(string $post): Collection
    {
        return collect(config('blog.locales'))
            ->mapWithKeys(function ($locale) use ($post) {
                return [$locale => new Models\Post($post, $locale)];
            })
            ->filter(function ($item) {
                return $item->exists();
            });
    }
}
