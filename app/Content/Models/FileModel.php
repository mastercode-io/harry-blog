<?php

namespace App\Content\Models;

use Storage;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Database\Eloquent\Model;
use Spatie\YamlFrontMatter\Document;
use Spatie\YamlFrontMatter\YamlFrontMatter;

class FileModel implements Arrayable
{
    /**
     * Determine if the post exists.
     *
     * @var boolean
     */
    protected $exists = false;

    /**
     * Determine if the post content is valid.
     *
     * @var boolean
     */
    protected $valid = false;

    /**
     * Slug representing the post.
     *
     * @var string
     */
    public $slug;

    /**
     * When the post was created.
     *
     * @var string
     */
    public $date;

    /**
     * The locale the model is for.
     *
     * @var string
     */
    public $locale;

    /**
     * Filename of the post.
     *
     * @var string
     */
    protected $filename;

    /**
     * Original markdown document.
     *
     * @var Document
     */
    protected $original;

    /**
     * Filesystem disk.
     *
     * @var Filesystem
     */
    protected $disk;

    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * Attributes of the post.
     *
     * @var array
     */
    protected $attributes = [];

    public function __construct(string $post, string $locale)
    {
        $this->disk = Storage::disk(config('blog.disk'));
        $this->locale = $locale ?? config('blog.default_locale');
        $this->filename = "{$post}/{$locale}.md";

        $this->decodeInfoFromPath($post);
        $this->readFile();
    }

    /**
     * Return
     *
     * @return void
     */
    protected function decodeInfoFromPath(string $path)
    {
        [$date, $slug] = explode('.', $path, 2);

        $this->slug = $slug;
        $this->date = Carbon::createFromFormat('Y-m-d', $date);
    }

    /**
     * Get the original markdown document.
     *
     * @return Document
     */
    public function getOriginal(): Document
    {
        return $this->original;
    }

    /**
     * Determine if the post exists on disk.
     *
     * @return boolean
     */
    public function exists(): bool
    {
        return $this->exists;
    }

    /**
     * Read the post file.
     *
     * @return void
     */
    protected function readFile()
    {
        if (!$this->disk->exists($this->filename)) {
            return null;
        }

        $this->exists = true;

        $this->attributes = array_merge($this->attributes, array_only($this->parseFileContent(), $this->fillable));

        $this->valid = true;
    }

    /**
     * Parse markdown content of the post file.
     *
     * @param mixed $file
     * @return array
     */
    protected function parseFileContent(): array
    {
        $this->original = YamlFrontMatter::parse($this->disk->get($this->filename));

        return array_merge($this->original->matter(), ['body' => markdown($this->original->body())]);
    }

    /**
     * Mass define model's attributes.
     *
     * @param array $data
     * @return void
     */
    public function fill(array $data)
    {
        $editable = array_only($data, $this->fillable);

        foreach ($editable as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * Save the post to the file.
     *
     * @return void
     */
    public function save()
    {
        // TODO: logic for saving the file.
    }

    /**
     * Transform the post into an array.
     *
     * @return array
     */
    public function toArray()
    {
        $defaults = [
            'slug'   => $this->slug,
            'date'   => $this->date,
            'locale' => $this->locale,
            'path'   => $this->filename,
        ];

        return array_merge($defaults, $this->attributes);
    }

    /**
     * Return the value of specified attribute.
     *
     * @param [type] $key
     * @return void
     */
    public function getAttribute($key)
    {
        if (!$key) {
            return;
        }

        $method = Str::camel("get-{$key}-attribute");

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        if (array_key_exists($key, $this->attributes)) {
            return $this->attributes[$key];
        }

        return;
    }

    /**
     * Set value of the specified attribute.
     *
     * @param [type] $key
     * @param [type] $value
     * @return void
     */
    public function setAttribute($key, $value)
    {
        if (!$key) {
            return;
        }

        $method = Str::camel("set-{$key}-attribute");

        if (method_exists($this, $method)) {
            $this->$method($value);
            return;
        }

        $this->attributes[$key] = $value;
    }

    /**
     * @param string $property
     * @return void
     */
    public function __get($property)
    {
        return $this->getAttribute($property);
    }

    /**
     * @param string $property
     * @param mixed  $value
     */
    public function __set($property, $value)
    {
        $this->setAttribute($property, $value);
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return !is_null($this->getAttribute($key));
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        unset($this->attributes[$key]);
    }
}
