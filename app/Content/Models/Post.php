<?php

namespace App\Content\Models;

class Post extends FileModel
{
    /**
     * Fillable attributes.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'summary',
        'body',
        'category',
        'external_url',
        'preview_image',
    ];

    /**
     * Get the post URL.
     *
     * @return void
     */
    public function getUrlAttribute()
    {
        return route('posts.show', [config('app.locale'), $this->slug]);
    }
}
