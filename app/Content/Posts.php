<?php

namespace App\Content;

use Illuminate\Support\Facades\Facade;

class Posts extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'postsManager';
    }
}
