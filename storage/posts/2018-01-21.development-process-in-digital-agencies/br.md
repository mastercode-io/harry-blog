---
title: Processo de desenvolvimento em agências digitais - Estudo e Proposta
summary: Nesta série de posts, apresento dados de pesquisa sobre metodologias existentes, processos utilizados por agências e, com base nesses dados e em minha experiência, proponho caminhos para melhorar o processo de desenvolvimento de aplicações nesses ambientes.
---

