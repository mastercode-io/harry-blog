---
title: Developing a simple blog with Laravel
summary: An in-detail view about how to build a blog using Laravel, Markdown and SQLite. Find out how fast and easy it can be.
---

I wanted to have my own blog for a long time. When the time had come I had to decide what platform I would get into. I didn't want
to stretch with Wordpress, primarily because it would be like kill an ant with a tank. Ghost looked like a great option, as Canvas, a
Laravel based blog platform, did too. But the idea of developing my own simple blog platform was surrounding in my head. The question
is over. My first blog post ever is about how I built my first blog ever.

This is a summary of what we are going to use to develop it:

*   [Laravel](https://www.laravel.com/)
*   SQLite
*   Some Markdown library
*   [Tailwind](https://tailwindcss.com/) for CSS stuff
*   [Vue](https://www.vue.org/) for JavaScript-ish

## Foundation

Let's get start by creating a laravel project. If you have the Laravel CLI installed, just run:

```
laravel new blog
```

You can use `composer` to create it:

```
composer create-project --prefer-dist laravel/laravel blog
```

When Laravel code is set on, `cd blog` and let's add some stuff. Type in your terminal:

```
composer require laravel-frontend-presets/tailwindcss league/commonmark
php artisan preset tailwindcss-auth
```

It will install a Laravel package to add frontend presets based on TailwindCSS. Tailwind is an utility-first css library. We could use Bootstrap to make things easier, but I rather like to use Tailwind because you have a bit more control on your CSS styles. It will also install League's Commonmark to convert Markdown to HTML. Last command will install the front-end presets using Tailwind.

We just installed some PHP stuff, let's install some front-end stuff now. When you did run `php artisan preset tailwindcss-auth`, Laravel updated _package.json_. Let's install the pre-configured packages.

> I will be using YARN across all this post. You can use NPM as well. It's up to you!

```sh
$ yarn
$ yarn run dev
```

Running `yarn` you are installing our front-end dependencies. Next command `yarn run dev` compile the assets as defined in _webpack.mix.js_. Now open the blog on your browser and you will see Laravel default page. We have our foundation set and up.

## Database

Our blog platform will be as simple as possible, so we don't have to much to think about Database planning and stuff like that. Anyway we are going to generate some migrations to prepare our SQLite database. First step is to open `.env` file and ensure that Database setings is correctly set up. It should look like:

```
DB_CONNECTION=sqlite
```

Now, run `php artisan migrate create_posts_table --create=posts` to create our migration file amd get the following content to it:
